import React, { useState } from 'react';
import { Button, View , Text, Alert , StyleSheet } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

export default function Login({ navigation }){

    const [usuario, asignarUser] = useState("");
    const [pass, asignarPass] = useState("");
    const [data, setData] = useState([]);
    const [isLoading, setLoading] = useState(false);

    function comprobarLogin(){
        var loading = true;
        var res = [];
        let formData = new FormData();
        formData.append("user",usuario);
        formData.append("pass",pass);
        fetch('https://moviles.xpcloud.com.mx/?code=login-acceso-juego', {
          method: 'POST',
          body: formData
      })
            .then((response) => response.json())
            .then((json) =>  setData(json))
            .catch((error) => console.error(error))
            .finally(() => loading = false);
            console.log(data);
            if(data.code == 200){
              Alert.alert(data.msg);
              navigation.navigate('Menu',{
                idu: data.usuario.id
            });
            }else {
              Alert.alert(data.msg);
            }
    }
    return (
        <View style={styles.container}>
            
            
            
            <Text style={{textAlign:"center",fontSize:20,marginBottom:20,color:"white"}}>Iniciar Sesión</Text>
            <TextInput 
                style = {styles.input}
                onChangeText={usuario=>asignarUser(usuario)}
                placeholder="Ingrese su usuario"
            />
            <TextInput 
                style = {styles.input}
                textContentType="password"
                secureTextEntry={true}
                onChangeText={pass=>asignarPass(pass)}
                placeholder="Ingrese Contraseña"
            />
            
          <TouchableOpacity
            style={{backgroundColor:"#ed6663"}}
           
            onPress={comprobarLogin }
          >
            <Text style={{color:"white",textAlign:"center",paddingTop:10,paddingBottom:10}}>Acceder</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Registro')}>
                <Text style={{textAlign:"center",fontSize:20,margin:20,color:"white"}}>Registrarme</Text>
            </TouchableOpacity>
             
        </View>
      );
}

const styles = StyleSheet.create({
  container:{ flex: 1, justifyContent: 'center', paddingLeft:20,paddingRight:20,backgroundColor:"#1b262c", },
    input : {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1 ,
        borderRadius:10,
        marginBottom:20,
        paddingStart:10,
        color:"white"
    }
});