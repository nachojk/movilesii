import * as React from 'react';
import { Button, View , StyleSheet,TouchableOpacity ,Text} from 'react-native';

export default function Menu({ route, navigation }){
  const { itemId } = route.params;
  const { idu } = route.params;
  const iduser = idu;
  console.log(idu + "usuario");

    return (
        <View style={styles.container}>
          <TouchableOpacity style={styles.boton}  onPress={() => navigation.navigate('Game',{
              id_word : 1,
              palabra : 'mouse',
              idu:iduser
            })}>
              <Text style={{color:"#ffffff",textAlign:"center",fontSize:20}}>Jugar Ahora</Text>
              </TouchableOpacity>
          <TouchableOpacity style={styles.boton}   onPress={() => navigation.navigate('Categorias',
            {idu:iduser}
          )}>
            <Text style={{color:"#ffffff",textAlign:"center",fontSize:20}}>Categorias</Text>
            </TouchableOpacity>        
          <TouchableOpacity style={styles.boton}   onPress={() => navigation.goBack()} >
            <Text style={{color:"#ffffff",textAlign:"center",fontSize:20}}>Cerrar Sesión</Text>
          </TouchableOpacity>
        </View>
      );
}

const styles = StyleSheet.create({
  container :{ 
    flex: 1, 
    justifyContent: 'center' ,
    paddingLeft:20,
    paddingRight:20
    ,backgroundColor:"#1b262c"
  },
  boton:{
    marginBottom:15,
     backgroundColor:"#0f4c81",
     borderRadius:10,
     padding:10,
    paddingLeft:60,
    paddingRight:60,
    borderRadius:10,
    margin:10
  }
});