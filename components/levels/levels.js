import React ,{useState, useEffect} from 'react';
import { StyleSheet,View,Text, Button,FlatList,TouchableOpacity,Image,ActivityIndicator} from 'react-native';





export default function Niveles({navigation, route}){
    const { idCat } = route.params;
    const idCategoria =idCat;
    const {idu} = route.params;
    const nav=navigation;
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
      
      const url = "https://moviles.xpcloud.com.mx/?code=ver-palabras-categoria-juego&idCat=" + idCategoria + "&idu=" + idu;
     
      fetch(url)
        .then((response) => response.json())
        .then((json) => setData(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
      
    }, []);

    return(
        <View style={styles.MainContainer}>
        {isLoading ? <ActivityIndicator/> : (
        <FlatList
        data={data}
        numColumns={3}
        horizontal={false}
        renderItem={({ item }) => (
          <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
            <TouchableOpacity onPress={() => navigation.navigate('Game',{
              id_word : item.id_palabra,
              palabra : item.palabra,
              idu :idu
            })}>
              <Image style={styles.imageThumbnail} source={{ uri: item.src }} />
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(item, index) => index}
      />
      )}
      </View>
    )
}
const styles = StyleSheet.create({
    MainContainer: {
      justifyContent: 'center',
      flex: 1,
      backgroundColor:"#1b262c"
    },
  
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 100,
      margin: 15,
      borderRadius: 15,
      borderWidth:1,
      borderColor: '#000'
    },
    cajon: {
      padding:10,
      paddingLeft:60,
      paddingRight:60,
      borderRadius:10,
      backgroundColor:"#000",
      margin:10
  },
  texto_cajon:{
      textAlign:"center",
      color:"#ffffff",
      fontSize:20
  }
  });
