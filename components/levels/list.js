import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';


export default class Lista extends Component {
  constructor() {
    super();
    this.state = {
      dataSource: {},
    };
    this.opciones=this.opciones.bind(this);
  }
  componentDidMount() {     //Aqui se realizara conexion a la base de datos para ver cuantos niveles tiene la categoria
    var that = this;
    let items = Array.apply(null, Array(60)).map((v, i) => {
      return { id: i, src: 'http://placehold.it/200x200?text=' + (i + 1) };
    });
    that.setState({
      dataSource: items,
    });
  }
  opciones(op){
    console.log(op);
    this.props.navigation.navigate('Juego',op);
  }
  render() {
    return (
      <View style={styles.MainContainer}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
              <TouchableOpacity onPress={()=>this.opciones(item.id)}>
                <Image style={styles.imageThumbnail} source={{ uri: item.src }} />
              </TouchableOpacity>
            </View>
          )}
          numColumns={3}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
  },

  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    margin: 15,
    borderRadius: 15,
    borderWidth:1,
    borderColor: '#000'
  },
});