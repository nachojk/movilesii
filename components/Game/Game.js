import  React , {Component} from 'react';
import { Button, View , StyleSheet , FlatList ,Image } from 'react-native';


const imagenes = [
    {'img':'https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg'},
    {'img':'https://www.imago-images.de/imagoextern/bilder/stockfotos/imago-images-geniale-luftaufnahmen.jpg'},
    {'img':'https://i.pinimg.com/originals/ca/76/0b/ca760b70976b52578da88e06973af542.jpg'},
    {'img':'https://i.pinimg.com/originals/9e/d8/6f/9ed86f4d59363daf10f67d41282cab6b.jpg'},
]
const letras = [
    {'letra':1}
]

 class Game extends Component{
    _renderItem(item){
        return(
            <Image 
                style={styles.img}
                source={{uri : item.img}}
            />
        )
    }
    render(){
       return( <View style={styles.container}>
        <FlatList
            data={imagenes}
            renderItem={({item})=>this._renderItem(item)}
           numColumns={2}
        />
    </View>)
    }
    

}

const styles = StyleSheet.create({
  container :{ 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },
  img:{
      margin:10,
      width:120,
      height:180
  }
});

export default Game