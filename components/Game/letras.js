import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';


export default class Letas extends Component {
  palabraescrita = "";
  constructor(props) {
    super(props);
    
    this.state = {
      cargando : false,
      dataSource: [],
      letters: []
    };
  }
  componentDidMount() {     //Aqui se realizara conexion a la base de datos para ver las categorias que existen
    var that = this;
    //this.getPalabra("1");
    var letra = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    var letras = letra.split('');
    
    var aux=this.props.palabra.toUpperCase();
    that.setState({letters :aux.split('')});
    
    var numLetr1 = letras[parseInt(Math.floor(Math.random() * 22))] ;
    var numLetr2 = letras[parseInt(Math.floor(Math.random() * 22))] ;
    var numLetr3 = letras[parseInt(Math.floor(Math.random() * 22))] ;
    
    var nueva = this.props.palabra.toUpperCase() + numLetr1 + numLetr2 + numLetr3 + "" ;

    var arrss = shuffle(nueva.split(''));
  
   
    let items = Array.apply(null, Array(nueva.length)).map((v, i) => {
   
      return { letra : arrss[i] , id:i ,color: null};
    });
    that.setState({
      dataSource: items,
    });
  }

  verificar(letra,id){
    var palabra=this.state.letters;
    var aux=[]
    if(letra==palabra[0]){
      for (let i = 0; i < this.state.dataSource.length; i++) {
        if(this.state.dataSource[i].id==id){
          if(this.state.dataSource[i].color==null){
            this.state.dataSource[i].color='green';
            palabra.shift();
          }
        }
        aux.push(this.state.dataSource[i]);
      }
      this.setState({letters : palabra});
      this.setState({dataSource:aux});
      if(palabra.length==0){
        Alert.alert("¡Nivel completado!");
        this.props.nav.goBack();
        var cat = this.props.categoria;
        var pal = this.props.pal;
        const url = "https://moviles.xpcloud.com.mx/?code=pasar-siguiente-nivel&id_cat="+cat+"&id_pal="+pal+"$idu=2";
        async ()=>{
          const response=  fetch(url);
          const json=  response.json();
          console.log('Nuevo nivel');
           console.log(json);
          this.props.nav.push('Game',{
            id_word : json.id_palabra,
            palabra : json.palabra
          });
        }
        
      }
    }
    
  }
    
  render() {

    return (
      <View style={styles.MainContainer}>
          <Text style={styles.texto}>{this.props.palabraescrita}</Text>
        <FlatList
          style={styles.flat}
          data={this.state.dataSource}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
              <TouchableOpacity  style={[styles.imageThumbnail, {backgroundColor: this.state.dataSource[item.id].color}]}
              onPress={()=>{
                this.verificar(item.letra,item.id)
              }}
              >
                  <Text style={{color:"white"}}>{item.letra}</Text>
              </TouchableOpacity>
                
            </View>
          )}
          numColumns={(this.props.palabra.length+3)}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}
function shuffle(arr) {
  var i,
      j,
      temp;
  for (i = arr.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
  }
  return arr;    
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#0f4c81',
  },
  texto:{
    fontSize: 25,
    textAlign:"center",
    marginTop: 10,
    marginBottom: 10,
  },

  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:'center',
    height: 50,
    borderRadius: 15,
    borderWidth:1,
    borderColor: '#fff',
    backgroundColor: null
  },
  imageThumbnailC: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:'center',
    height: 50,
    borderRadius: 15,
    borderWidth:1,
    borderColor: '#fff',
    backgroundColor: '#f2a365'
  },
  flat:{
    marginRight:10,
    marginLeft:10
  }
});