import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';


/*const imagenes = [
  {'img':'https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg'},
  {'img':'https://www.imago-images.de/imagoextern/bilder/stockfotos/imago-images-geniale-luftaufnahmen.jpg'},
  {'img':'https://i.pinimg.com/originals/ca/76/0b/ca760b70976b52578da88e06973af542.jpg'},
  {'img':'https://i.pinimg.com/originals/9e/d8/6f/9ed86f4d59363daf10f67d41282cab6b.jpg'},
]*/

var imagenes=[]

export default class Lista extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: {},
    };
  }

  componentDidMount() {     //Aqui se realizara conexion a la base de datos para ver las categorias que existen
    imagenes=[];      //Si no lo reinicio las imagenes que se hayan mandado antes siguen
    imagenes.push({ img: 'https://moviles.xpcloud.com.mx/' + this.props.img1 });
    imagenes.push({ img: 'https://moviles.xpcloud.com.mx/' + this.props.img2 });
    imagenes.push({ img: 'https://moviles.xpcloud.com.mx/' + this.props.img3 });
    imagenes.push({ img: 'https://moviles.xpcloud.com.mx/' + this.props.img4 });
    /*imagenes = Array.apply(null, Array(4)).map((v, i) => {
      return { img: 'https://moviles.xpcloud.com.mx/' + imag[i] };
    });*/
    console.log('Links: ');
    console.log(imagenes);
  }
  render() {
    return (
      <View style={styles.MainContainer}>
        <FlatList
          data={imagenes}
          horizontal={false}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
                <Image style={styles.imageThumbnail} source={{ uri: item.img }} />
            </View>
          )}
          numColumns={2}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
  imagenes=[]
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 2,
    justifyContent: 'center',
    backgroundColor: '#0f4c81',
  },

  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    margin: 15,
    paddingBottom: 10,
    borderRadius: 15,
    borderWidth:1,
    borderColor: '#000'
  },
});