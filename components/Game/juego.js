import React ,{useState, useEffect}  from 'react';
import { StyleSheet,View,Text, Button, ActivityIndicator} from 'react-native';
import Lista from './list';
import Letras from './letras';


export default function Juego({navigation,route}){
    const { id_word , palabra} = route.params;
    console.log('Palabra: '+palabra);
    const nav=navigation;
    const [isLoading, setLoading] = useState(true);
    const [wordRes, setData] = useState([]);
    console.log(wordRes);//Se supone que aqui va la info de la peticion, pero en la consola aparece vacio
    

    useEffect(() => {
      
      const url = "https://moviles.xpcloud.com.mx/?code=juego-play&idPal=" + id_word;
     
      fetch(url)
        .then((response) => response.json())
        .then((json) => setData(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
      
    }, []);
    console.log('Hola'); //Cuando imprime Hola significa que ya va a mostrar las imagenes y como esta vacio el wordRes manda vacio al componente y no muestra las imagenes
    
    return(
      isLoading ? <ActivityIndicator/> : (
        <View style={styles.container}>
        
          <Text style={styles.title}>Juego</Text>
          <Lista img1={wordRes.img1} img2={wordRes.img2} img3={wordRes.img3} img4={wordRes.img4}/>
          <Letras palabra={palabra} nav={navigation} categoria={wordRes.id_categoria} pal={wordRes.id_palabra}/>
      </View>
      )

    )
    
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#0f4c81',
  },
  title: {
    fontSize: 35,
    marginTop: 20,
    marginBottom: 25,
    textAlign: 'center',
    color:"white"
  }
});