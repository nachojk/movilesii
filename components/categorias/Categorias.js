import React, { Component, useEffect,useState } from 'react';

import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';

export default function Categorias({route, navigation}) {
    const { idu } = route.params;
    const [cargando, setLoading] = useState(true);
    const [resultado, setData] = useState([]);

    useEffect(() => {
        fetch("https://moviles.xpcloud.com.mx/?code=ver-categorias-juego")
        .then(res => res.json())
        .then(res => {
            setData(res);
        });
    }, []);
   
             return(
                <View style={styles.container}>
                    <FlatList
                        
                        data={resultado}
                        renderItem={ ({item}) => 
                        <TouchableOpacity style={styles.cajon}  
                        onPress={() =>  navigation.navigate('Niveles',{
                            idCat : item.id_categoria,
                            idu : idu
                        })}
                        >
                            <Text style={styles.texto_cajon}>{item.nombre}</Text>
                        </TouchableOpacity> 
                    }
                        keyExtractor={(item,index) => index.toString()}
                    />
                </View>
            );
        
        
    }

const styles =StyleSheet.create({
    container:{
        paddingLeft:20,
        paddingRight:20,
        flex:1,justifyContent:"center",paddingTop:20,backgroundColor:"#1b262c"
    },
    cajon: {
        padding:10,
        paddingLeft:60,
        paddingRight:60,
        borderRadius:10,
        backgroundColor:"#ed6663",
        margin:10
    },
    texto_cajon:{
        textAlign:"center",
        color:"#ffffff",
        fontSize:20
    }
});
