import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View, Button ,Alert,StyleSheet} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';



export default function Registro ({ navigation }){
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const [nombre, setName] = useState("");
  const [user, setUser] = useState("");
  const [pass, setPass] = useState("");
  const [correo, setCorreo] = useState("");

  useEffect(() => {
    
  }, []);

  function registro(){
    var loading = true;
    var res = [];
    let formData = new FormData();
    formData.append("nombre",nombre);
    formData.append("user",user);
    formData.append("pass",pass);
    formData.append("correo",correo);
    fetch('https://moviles.xpcloud.com.mx/?code=registro-usuario-juego', {
      method: 'POST',
      body: formData
  })
        .then((response) => response.json())
        .then((json) =>  setData(json))
        .catch((error) => console.error(error))
        .finally(() => loading = false);
        if(data.code == 200){
          Alert.alert(data.msg);
          navigation.goBack();
        }else {
          Alert.alert(data.msg);
        }
  }
  


  return (
    <View style={{ flex: 1, padding: 24,backgroundColor:"#1b262c" }}>
      
      <Text style={{textAlign:"center",fontSize:20,marginBottom:20,color:"white"}}>Crea tu cuenta</Text>
      <TextInput
      style={styles.input}
      placeholder="Ingrese su Nombre"
      onChangeText={nombre=>setName(nombre)}
      />
      <TextInput
      style={styles.input}
      placeholder="Ingrese Usuario"
      onChangeText={user=>setUser(user)}
      />
      <TextInput
      style={styles.input}
      placeholder="Ingrese Contraseña"
      secureTextEntry={true}
      textContentType="password"
      onChangeText={pass=>setPass(pass)}
      />
      <TextInput
      style={styles.input}
      placeholder="Ingrese Correo"
      textContentType="emailAddress"
      onChangeText={correo=>setCorreo(correo)}
      />
      <Button
      title="Registrarse"
      onPress={registro}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  input : {
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1 ,
      borderRadius:10,
      marginBottom:20,
      paddingStart:10,

  }
});