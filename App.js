import * as React from 'react';
import { Button, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './components/Login/Login';
import Menu from  './components/Menu/Menu';
import GameStar from  './components/Game/juego';
import Categorias from './components/categorias/Categorias';
import Levels from './components/levels/levels';
import Registro from './components/registro/Registro';
const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home"  component={Login}  options={{ title: 'Iniciar Sesión' }} />
      <Stack.Screen name="Registro" component={Registro} />
      <Stack.Screen name="Menu" component={Menu} />
      <Stack.Screen name="Game" component={GameStar} />
      <Stack.Screen name="Categorias" component={Categorias} />
      <Stack.Screen name="Niveles" component={Levels} />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}
